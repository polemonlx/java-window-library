package org.polemon.java_window_library.window;

public class WindowTest {

    private final Window window;


    public WindowTest() {
        window = Window.getInstance();
    }


    public boolean displayTest() throws Exception {
        return window.getFrame() != null;
    }


    public boolean dimensionsTest() throws Exception {

        if (window.getFrame().getWidth() != 350) {
            return false;
        }
        if (window.getFrame().getHeight() != 350) {
            return false;
        }
        if (!window.getFrame().getTitle().equals("Java Window Library")) {
            return false;
        }

        return true;
    }

}
