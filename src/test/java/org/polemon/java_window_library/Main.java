package org.polemon.java_window_library;

import org.polemon.java_window_library.graphic.RectangleTest;
import org.polemon.java_window_library.window.WindowTest;

import static java.lang.System.out;

public class Main {

    private final int TEST_DELAY = 1000;


    public static void main(String[] args) {

        Main main = new Main();

        try {

            WindowTest windowTest = new WindowTest();
            main.printTest("Init window", windowTest.displayTest());
            main.printTest("Window dimensions", windowTest.dimensionsTest());

            RectangleTest rectangleTest = new RectangleTest();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void printTest(String message, Boolean validation) throws InterruptedException {
        out.print(message + ": ");
        Thread.sleep(TEST_DELAY);
        out.println(validation ? "OK" : "FAIL");
    }

}
