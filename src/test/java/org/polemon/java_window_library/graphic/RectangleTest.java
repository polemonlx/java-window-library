package org.polemon.java_window_library.graphic;

import org.polemon.java_window_library.window.Window;

public class RectangleTest {

    private Rectangle rectangle;
    private final int INIT_X = 50;
    private final int INIT_Y = 50;
    private final int INIT_WIDTH = 100;
    private final int INIT_HEIGHT = 100;


    public RectangleTest() {
        rectangle = new Rectangle(INIT_X, INIT_Y, INIT_WIDTH, INIT_HEIGHT);
    }


    public boolean initTest() {

        rectangle.draw();
        return Window.getInstance().getShapesCount() == 1;
    }


    public boolean dimensionTest() {
        return rectangle.getX() == INIT_X &&
                rectangle.getY() == INIT_Y &&
                rectangle.getWidth() == INIT_WIDTH &&
                rectangle.getHeight() == INIT_HEIGHT;
    }


    public boolean fillTest() {
        rectangle.fill();
        return rectangle.isFilled();
    }


    public boolean translateTest() {
        rectangle.translate(-20, 50);
        return rectangle.getX() == INIT_X - 20 &&
                rectangle.getY() == INIT_Y + 50 &&
                rectangle.getWidth() == INIT_WIDTH &&
                rectangle.getHeight() == INIT_HEIGHT;
    }

    public boolean

}
