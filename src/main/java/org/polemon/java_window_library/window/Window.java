package org.polemon.java_window_library.window;

import javax.swing.*;
import java.awt.image.BufferedImage;

public class Window {

    private static final Window window = new Window();
    private static final int WINDOW_SIZE = 350;
    private static final int WINDOW_LOCATION = 120;
    private static final String WINDOW_TITLE = "Java Window Library";

    private BufferedImage background;
    private final JFrame frame;


    private Window() {

        frame = new JFrame(WINDOW_TITLE);
        frame.setSize(WINDOW_SIZE, WINDOW_SIZE);
        frame.setLocation(WINDOW_LOCATION, WINDOW_LOCATION);

        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        frame.setVisible(true);
    }


    /**
     * Access to singleton Window.
     *
     * @return Window
     */
    public static Window getInstance() {
        return window;
    }


    /** Access to JFrame of the window.
     *
     * @return JFrame
     */
    public JFrame getFrame() {
        return frame;
    }

}
