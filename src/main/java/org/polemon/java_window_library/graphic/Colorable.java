package org.polemon.java_window_library.graphic;

import java.awt.*;

/**
 * Methods for modifying a shape color
 * The color of the shape is used when drawing or filling
 */
public interface Colorable {

    /**
     * Sets the color of the shape
     *
     * @param color the new color
     */
    void setColor(Color color);
}
