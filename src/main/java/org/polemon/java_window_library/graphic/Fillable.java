package org.polemon.java_window_library.graphic;

/**
 * Methods for filling a shape
 */
public interface Fillable {

    /**
     * Paints the shape with the current shape color
     */
    void fill();

}
